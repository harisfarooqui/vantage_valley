#!/usr/bin/python

import sys
import http.client 
import json

class Constants(object):
    def CHICAGO_ZC(self):
        return [60601,60602,60603,60604,60605,60606,60607,60608,60609,60610,60611,60612,60613,60614,60615,60616,60617,60618,60619,60620,60621,60622,60623,60624,60625,60626,60628,60629,60630,60631,60632,60633,60634,60636,60637,60638,60639,60640,60641,60642,60643,60644,60645,60646,60647,60649,60651,60652,60653,60654,60655,60656,60657,60659,60660,60661,60699,60701,60706,60707,60803,60804,60805,60827]
    def PITTSBURG_ZC(self):
        return [15106,15120,15201,15203,15204,15205,15206,15207,15208,15210,15211,15212,15213,15214,15215,15216,15217,15218,15219,15220,15221,15222,15224,15226,15227,15232,15233,15234,15235,15236,15238,15260,15290]
    def API_KEY(self):
        return '6b3b919c64f48c27b3f3c7042eaf5e14'

class SalesTrends(object):
    
    def __init__(self):
        self.constant = Constants()
        self.conn = http.client.HTTPSConnection("search.onboard-apis.com") 
        self.headers = { 
            'accept': "application/json", 
            'apikey': self.constant.API_KEY()
        } 

    def getSalesTrends(
        self,
        zipCodes=None,
        salesYear=2018
        ):
 
        for zipCode in zipCodes:
            self.conn.request("GET", "/propertyapi/v1.0.0/salestrend/snapshot?geoid=ZI"+ str(zipCode) +"&interval=monthly&startyear="+ str(salesYear) +"&endyear=" + str(salesYear) + "&startmonth=january&endmonth=december", headers=self.headers)
            res = self.conn.getresponse() 
            data = res.read() 
            jsonObject = json.loads(data)
            salesTrends = jsonObject['salestrends']
            for salesTrend in salesTrends:
                print(str(salesTrend['location']['geoID'])+ "|" + salesTrend['daterange']['start'] + "|" + str(salesTrend['SalesTrend']['homesalecount']) + "|" + str(salesTrend['SalesTrend']['avgsaleprice']) + "|" + str(salesTrend['SalesTrend']['medsaleprice']))
            pass
        pass

if __name__ == "__main__":

    if len(sys.argv) < 3:
        print("Error executing script. Invalid arguments")
        print("Usage: python3 vv_propertySearch [CITY] [SALE_YEAR]")
        print("[CITY] - Valid values: Chicago, Pittsburg")
        print("[SALE_YEAR] - Valid values: 2010-2018")
        exit()

    constant = Constants()
    if (sys.argv[1:][0].upper() == "CHICAGO"):
        zipCodes = constant.CHICAGO_ZC()
    elif (sys.argv[1:][0].upper() == "PITTSBURG"):
        zipCodes = constant.PITTSBURG_ZC()
    else:
        print("Invalid City. Valid values: Chicago, Pittsburg")
        exit()

    if 2010 < int(sys.argv[2:][0]) > 2018:
        print("Invalid Sales year. Valid values: 2010-2018")
        exit()
    else:
        salesYear = sys.argv[2:][0]

    print(salesYear)
    salesTrend = SalesTrends()
    salesTrend.getSalesTrends(zipCodes,salesYear)